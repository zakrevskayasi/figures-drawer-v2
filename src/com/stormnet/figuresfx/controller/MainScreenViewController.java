package com.stormnet.figuresfx.controller;

import com.stormnet.figuresfx.drawutils.Drawer;
import com.stormnet.figuresfx.figures.Circle;
import com.stormnet.figuresfx.figures.Figure;
import com.stormnet.figuresfx.figures.Rectangle;
import com.stormnet.figuresfx.figures.Triangle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;

public class MainScreenViewController implements Initializable {
//    private Figure[] figures;
    List<Figure> figures = new ArrayList<>();
    private Random random;

    @FXML
    private Canvas canvas;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
//        figures = new Figure[1];
        random = new Random(System.currentTimeMillis());
    }

    @FXML
    private void onMouseClicked(MouseEvent mouseEvent) {
        addFigure(createFigure(mouseEvent.getX(), mouseEvent.getY()));
        repaint();
    }

       /**
     * Метод, который будет перерисовывать фигуры.
     */
    private void repaint(){
        //Чистим канву. В метод чистки передаем координаты левого верхнего угла, ширину канвы и ее высоту
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        Drawer<Figure> drawer = new Drawer<>(figures);
        drawer.draw(canvas.getGraphicsContext2D());
    }

    /**
     * Координаты клика
     * @param x
     * @param y
     * @return
     *
     */
    private Figure createFigure(double x, double y) {
        Figure figure = null;
        //Тип фигуры
        switch (random.nextInt(3)) {
            case Figure.FIGURE_TYPE_CIRCLE:
                figure = new Circle(x, y, random.nextInt(4), Color.GREEN, random.nextInt(50));
                break;
            case Figure.FIGURE_TYPE_RECTANGLE:
                figure = new Rectangle(x, y, random.nextInt(4), Color.RED, random.nextInt(100), random.nextInt(100));
                break;
            case Figure.FIGURE_TYPE_TRIANGLE:
                figure = new Triangle(x, y, random.nextInt(4), Color.BLUE, random.nextInt(100));
                break;
            default:
                System.out.println("Unknown figure type");
        }
        return figure;
    }

    private void addFigure(Figure figure) {
        /**
         * Выполняем проверку: если последний элемент массива равен null, значит мы кладем туда нашу фигуру и делаем выход из метода
         * Если же это условие не выполняется, т.е., в массиве уже есть фигуры, мы создаем новый массив размерностью +1, копируем туда элементы из старого массива
         * и добавляем новую фигуру
         */

        figures.add(figure);
//        if (figures[figures.length - 1] == null) {
//            figures[figures.length - 1] = figure;
//            return;
//        }
//
//        Figure[] tmp = new Figure[figures.length + 1];
//        int index = 0;
//        for (; index < figures.length; index++) {
//            tmp[index] = figures[index];
//        }
//        tmp[index] = figure;
//        figures = tmp;
    }
}
